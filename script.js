const questions = [
  {
    question: "Melyik fegyver jelenik meg a filmben?",
    options: {
      a: "Lézerszablya",
      b: "Csákány",
      c: "Parittya",
      d: "Befőttes gumi"
    },
    answer: "a"
  }, 
  {
    question: "1+1",
    options: {
      a: "1",
      b: "2",
      c: "3",
      d: "4"
    },
    answer: "b"
  },
  {
    question: "Melyik kérdés a C?",
    options: {
      a: "Ez",
      b: "Ez",
      c: "Ez",
      d: "Ez"
    },
    answer: "c"
  },
  {
    question: "Melyik kör veszi fel idén a legmenőbb újoncokat?",
    options: {
      a: "KSZK",
      b: "Egyik sem",
      c: "Schdesign"
    },
    answer: "c"
  },
  {
    question: "Ehhez a kvízhez könnyen lehet utólag akárhány kérdést hozzáadni?",
    options: {
      a: "Igen",
      b: "Nem",
      c: "Igen, de csak 4 válaszlehetőséggel",
    },
    answer: "a"
  }
];


const quizContainer = document.getElementById('quiz');
const resultsContainer = document.getElementById('results');
const submitButton = document.getElementById('submit');
const retryButton = document.getElementById('retry');

function buildQuiz(){

  //storing HTML output
  const output = [];

  let questionNumber = 0;
  questions.forEach((currQuestion) => {
      console.log(currQuestion);

      //answer outputs
      const answersTemp = [];

      //for every answer for this question...
      for (currLetter in currQuestion.options){
        console.log(currLetter);
        //...store a radio button
        answersTemp.push(
          
          "<input type='radio' id='a"+questionNumber + currLetter+"' name='question"+questionNumber+"' value="+currLetter+">"+
          "<label for='a"+questionNumber + currLetter+"'>" +
          currLetter.toUpperCase() + ": " + currQuestion.options[currLetter] +
          "</label>"
        );
       
      }

      // OUTPUT the current QUESTION and ANSWERS
      output.push(
        "<div class='slide'>"+
          "<div class='question'>"+
            "<h2><strong>" + (questionNumber+1) + ". kérdés" + "</strong></h2>" + 
            "<h3>" + currQuestion.question + "<h3>" +
          "</div>"+
          "<div class='answers'>"+answersTemp.join('')+"</div>" +
        "</div>"
      );
      questionNumber++;
  });
  
  console.log(output);
  // combining the output list into one string of HTML, and displaying it
  quizContainer.innerHTML = output.join('');

}

function showResults(){
  // get answer containers
  const answerContainers = quizContainer.querySelectorAll('.answers');
  slides[currentSlide].classList.remove('active-slide');
  previousButton.style.display = 'none';
  submitButton.style.display = 'none';

  // how many did the user get correct

  let correctNr = 0;

  // for each question...
  let questionNumber = 0;
  questions.forEach( (currQuestion) => {
    //finding selected answer
    const answerContainer = answerContainers[questionNumber];
    const selector = 'input[name=question'+questionNumber+']:checked';
    // "|| {}" for blank
    const userAnswer = (answerContainer.querySelector(selector)||{}).value

    //if answer is correct
    if(userAnswer === currQuestion.answer){
      
      correctNr++;

      //color the answers green
      answerContainers[questionNumber].style.color = 'lightgreen';
    } else {
    //if answer is wrong or blank
      answerContainers[questionNumber].style.color = 'red';
    }
    questionNumber++;
  });


  slides[currentSlide].classList.remove('active-slide');

  let resultsOutput = "";

  if(questions.length === correctNr) {
    //all of the answers are correct
    resultsOutput = "<h2>Gratulálunk, minden kérdésre jól válaszoltál!</h2>";
    resultsOutput += "<h2>Ahhoz, hogy részt vehess nyereményjátékunkban, kérjük add meg e-mail címedet:</h2>";
    resultsOutput += "<form action='#'><input type='email' name='emailAddress' active focus placeholder='E-mail címed'><br>";
    resultsOutput += "<input type='submit' value='Elküldés'></form>";
  } else {
    resultsOutput = "<h2><strong>" + questions.length + "</strong> kérdésből <strong>" + correctNr + "</strong> kérdésre válaszoltál jól.</h2>"
    resultsOutput += "<h2>Ahhoz, hogy részt vehess nyereményjátékunkban, próbálkozz újra!</h2>";
    retryButton.style.display = "inline-block";
    
  }
  resultsContainer.innerHTML = resultsOutput;
  resultsContainer.style.opacity = "1";

}

function retryQuiz(){
  resultsContainer.style.opacity = 0;
  currentSlide = 1;
  retryButton.style.display = "none";
  showSlide(1);
  
}

// display quiz right away
buildQuiz();

// on submit, show results
submitButton.addEventListener('click', showResults);

//Pagination
const previousButton = document.getElementById("previous");
const nextButton = document.getElementById("next");
const slides = document.querySelectorAll(".slide");
let currentSlide = 0;

function showSlide(n) {
  slides[currentSlide].classList.remove('active-slide');
  slides[n].classList.add('active-slide');
  currentSlide = n;
  if(currentSlide === 0 || currentSlide === 1){
    previousButton.style.display = 'none';
    previousButton.innerHTML = 'Kvíz megkezdése';
  } else {
    previousButton.style.display = 'inline-block';
    previousButton.innerHTML = 'Előző kérdés';
  }

  if(currentSlide===slides.length-1){
    nextButton.style.display = 'none';
    submitButton.style.display = 'inline-block';
  } else {
    nextButton.style.display = 'inline-block';
    if(currentSlide!=0) {nextButton.innerHTML = 'Következő kérdés'};
    submitButton.style.display = 'none';
  }
}


showSlide(0);
nextButton.innerHTML = "Kvíz megkezdése";

function showNextSlide(){
  showSlide(currentSlide+1);
}

function showPreviousSlide(){
  showSlide(currentSlide-1);
}

previousButton.addEventListener("click", showPreviousSlide);
nextButton.addEventListener("click", showNextSlide);
retryButton.addEventListener("click", retryQuiz);
